FROM python:3.4
WORKDIR /app
COPY ./web_server.py /app/app.py
COPY ./requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8000
CMD python /app/app.py